import { action, observable } from 'mobx';
import { FieldState } from '../field/field.state';


class ListState {
    @observable
    items: string[] = [];
    @observable
    currentItem = new FieldState();

    @action
    addCurrentItem() {
        this.items.push(this.currentItem.value);
        this.currentItem.onChange('');
    }
    @action
    reset() {
        this.items = [];
        this.currentItem.onChange('');
    }
}

// export one instance of the state
export const listState = new ListState();
