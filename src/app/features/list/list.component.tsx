import React, { Component } from 'react';
import { observer } from 'mobx-react';

import { FieldComponent } from '../field/field.component';
import { listState } from './list.state';

@observer
export class ListComponent extends Component {
    render() {
        return (
            <section>
                <form onSubmit={e => {
                    e.preventDefault();
                    listState.addCurrentItem();
                }}>
                    <FieldComponent fieldState={listState.currentItem} />
                    <button type="submit">Add</button>
                    <button type="button" onClick={() => listState.reset()}>Reset</button>
                    <ul>
                        {listState.items.map((item, index) => {
                            return (
                                <li key={index}>{item}</li>
                            );
                        })}
                    </ul>
                </form>

            </section>
        );
    }
}
