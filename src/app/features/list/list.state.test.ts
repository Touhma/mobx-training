import { listState } from './list.state';


beforeEach(()=>{
    listState.reset();
})

test('Store a list of items ( strings )', () => {
    expect(listState.items).toEqual([]);
});

test('Allow maintaining a *current* string as it gets typed', () => {
    expect(listState.currentItem.value).toEqual('');

    listState.currentItem.onChange('a');
    expect(listState.currentItem.value).toEqual('a') ;

    listState.currentItem.onChange('ab');
    expect(listState.currentItem.value).toEqual('ab');
});

test('Ability to add that string to the list', () => {
    listState.currentItem.onChange('ab');
    listState.addCurrentItem();

    expect(listState.items).toEqual(['ab']);
});

test('Allow reset the items and the current string', () => {
    listState.reset();

    expect(listState.currentItem.value).toEqual('');
    expect(listState.items).toEqual([]);
});
