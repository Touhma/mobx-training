import React, { Component } from 'react';
import { observer } from 'mobx-react';

import { FieldState } from './field.state';

@observer
export class FieldComponent extends Component<{ fieldState : FieldState}> {
    render() {
        return (
            <section>
                <input value={this.props.fieldState.value}
                       onChange={event => this.props.fieldState.onChange(event.target.value)} />
            </section>
        );
    }
}
