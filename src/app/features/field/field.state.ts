import { action, observable } from 'mobx';

export interface FieldState {
    value: string ;
}


export class FieldState implements FieldState{
    @observable
    value = '';

    @action
    onChange(newValue : string){
        this.value = newValue;
    }
}
