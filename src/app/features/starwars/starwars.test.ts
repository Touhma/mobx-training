import { starwarsState } from './starwars.state';
import { charactersService } from './starwars.service';

beforeEach(() => {
    starwarsState.reset();
})

test('Store a list of characters from an API ( CharacterModel )', () => {
    expect(starwarsState.characters).toEqual([]);

    charactersService.fetchCharacters$.subscribe(data => {
        starwarsState.fetchCharactersSuccess(data);
        expect(starwarsState.characters).toEqual(data);
    });
});

test('Reset all data if needed', () => {
    starwarsState.reset();
    expect(starwarsState.characters).toEqual([]);
});
