import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { starwarsState } from './starwars.state';

@observer
export class StarwarsComponent extends Component {
    render() {
        return (
            <section>
                <div>
                    <ul>
                        {starwarsState.characters.map((item, index) => {
                            return (
                                <li key={index}>{item.name}</li>
                            );
                        })}
                    </ul>
                </div>
                <button onClick={() => starwarsState.fetchCharactersRequest()}>Load More</button>
            </section>
        );
    }
}

// Functional component with observer behaviour
/*
export const StarwarsComponent = observer(() => {
    return (
        <section>
            <div>
                <ul>
                    {starwarsState.characters.map((item, index) => {
                        return (
                            <li key={index}>{item.name}</li>
                        );
                    })}
                </ul>
            </div>
            <button onClick={() => starwarsState.fetchCharactersRequest()}>Load More</button>
        </section>
    );
});
 */
