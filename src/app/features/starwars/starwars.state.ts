import { action, observable } from 'mobx';
import { CharacterModel } from '../../model/character.model';
import { charactersService } from './starwars.service';

/**
 * Features :
 * - Store a list of characters from an API ( CharacterModel )
 * - Ability to modify a character from the list
 * - Ability to add a character to the list
 * - Ability to remove a character from the list
 * - Reset all data if needed
 */

interface StarwarsState {
    characters: Array<CharacterModel>;
    isLoading: boolean;
    isHavingError: boolean;
}

class StarwarsState implements StarwarsState {
    @observable
    characters = new Array<CharacterModel>();
    @observable
    isLoading = false;
    @observable
    isHavingError = false;


    @action
    fetchCharactersRequest() {
        this.isLoading = true;
        charactersService.fetchCharacters();
    }

    @action
    fetchCharactersSuccess(characters: Array<CharacterModel>) {
        this.characters = characters;
    }

    @action
    fetchCharactersError(err : Error) {
        this.isHavingError = true;
    }

    @action
    changeLoadingStatus(status : boolean){
        this.isLoading = status;
    }
    @action
    triggerAnError(err:Error){
        this.isHavingError = true;
        //Can be replaced by a state attribute error with an intractable UI Component
        console.log(err);
    }

    @action
    resetErrorTrigger(){
        this.isHavingError = false;
    }

    @action
    reset() {
        this.characters = new Array<CharacterModel>();
    }
}

export const starwarsState = new StarwarsState();
