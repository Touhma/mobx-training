import { Observable, of } from 'rxjs';
import rx from './../../configuration/rxjs.config'
import { starwarsState } from './starwars.state';
import { CharacterModel } from '../../model/character.model';

class CharactersService {
    api = '5e9f1d902d00007b00cb79eb';

    // Return the observable on the side to be able to test the code
    fetchCharacters$: Observable<Array<CharacterModel>> = rx.get<Array<CharacterModel>>(this.api);

    //Actual Function called by the action
    fetchCharacters() {
        starwarsState.changeLoadingStatus(true);
        this.fetchCharacters$.subscribe(
            data => {
                if (data) {
                    console.log("Character Service : Fetch Success")
                    starwarsState.fetchCharactersSuccess(data);
                }
            },
            err => {
                console.log("Character Service : Fetch Error");
                starwarsState.triggerAnError(err);
            },
            () => {
                starwarsState.changeLoadingStatus(false);
                console.log('Character Service : Fetch Complete')
            }
        );
    }
}

export const charactersService = new CharactersService();
