import { action, observable, computed } from 'mobx';

// if need only actions authorized on state
/*
configure({
    enforceActions: true
});
//*/

export class HelloState {
    //def d'un observable
    @observable clickedCount = 0;

    //Def d'une action
    @action
    increment() {
        this.clickedCount++;
    }

    // --> Optimisation : Only run the body if the value change, if not : return the last one
    @computed
    get hasBeenClicked(){
        console.log("hasBeenClicked")
        return this.clickedCount > 0;
    }
}
