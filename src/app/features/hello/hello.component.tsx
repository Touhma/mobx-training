import React, { Component } from 'react';
import { HelloState } from './hello.state';
import { observer } from 'mobx-react';

@observer
export class HelloComponent extends Component {
    data = new HelloState();

    render() {
        return (
            <section>
                <button onClick={() => this.data.increment()}>
                    Click Count = {this.data.clickedCount}
                </button>
                {
                    // Condition
                    this.data.hasBeenClicked
                    // result rendered
                    && <div>You have clicked it !</div>
                }
            </section>
        );
    }
}
