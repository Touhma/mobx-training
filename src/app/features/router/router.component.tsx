import React, { Component } from 'react';
/*
import { inject, observer } from 'mobx-react';
import { RouterStore } from 'mobx-react-router';
*/

import {  observer } from 'mobx-react';
import { routingStore } from './router.state';

@observer
export default class RouterComponent extends Component {
    render() {
        const { location, push, goBack } = routingStore;
        console.log("routing rendered");
        return (
            <div>
                <span>Current pathname: {location.pathname}</span>
                <button onClick={() => push('/')}>Home</button>
                <button onClick={() => push('/App')}>App</button>
                <button onClick={() => push('/starwars')}>Starwars</button>
                <button onClick={() => goBack()}>Go Back</button>
            </div>
        );
    }
}

// possibility of injection from a main store : cf --> index.tsx
/*
// Add that to reflect the type of props :

interface Props {
    routing: RouterStore
}

@inject('routing')
@observer
export default class RouterComponent extends Component<Props> {
    render() {
        const { location, push, goBack } = this.props.routing ;
        console.log("routing rendered");
        return (
            <div>
                <span>Current pathname: {location.pathname}</span>
                <button onClick={() => push('/')}>Home</button>
                <button onClick={() => push('/App')}>App</button>
                <button onClick={() => push('/starwars')}>Starwars</button>
                <button onClick={() => goBack()}>Go Back</button>
            </div>
        );
    }
}
*/
