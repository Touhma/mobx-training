export interface CountProps {
    clickedCount : number
}

export class CountProps implements CountProps{
    constructor() {
        this.clickedCount = 0;
    }
}
