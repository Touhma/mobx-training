import React from 'react';
import './app.scss';
import { HelloComponent } from './features/hello/hello.component';
import { ListComponent } from './features/list/list.component';
import {StarwarsComponent} from './features/starwars/starwars.component';

function AppComponent() {
    return (
        <div className="App">
            <header className="App-header">
                <HelloComponent />
                <ListComponent />
                <StarwarsComponent />
            </header>
        </div>
    );
}

export default AppComponent;
