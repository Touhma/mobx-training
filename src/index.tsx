import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import AppComponent from './app/app.component';

import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
import { Route, Router } from 'react-router';
import {createBrowserHistory} from 'history';
import { Provider } from 'mobx-react';
import { HelloComponent } from './app/features/hello/hello.component';
import { StarwarsComponent } from './app/features/starwars/starwars.component';
import RouterComponent from './app/features/router/router.component';
import { routingStore } from './app/features/router/router.state';

const browserHistory = createBrowserHistory();

//Possibility of global store like that
/*
const stores = {
    routing: routingStore,
    // ...other stores
};
--> Replace in RouterComponent :

--> Replace in the render method :
<Provider {...stores}>
        <Router history={history}>
            <RouterComponent routing={stores.routing} />

*/
const history = syncHistoryWithStore(browserHistory, routingStore);

//Remove strict mode around app in prod to avoid double rendering  <React.StrictMode>
ReactDOM.render(
    <Provider>
        <Router history={history}>
            <RouterComponent />
            <Route path="/" exact component={HelloComponent} />
            <Route path="/App" exact component={AppComponent} />
            <Route path="/starwars" component={StarwarsComponent} />
        </Router>
    </Provider>
    , document.getElementById('root')
);

