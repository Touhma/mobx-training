## What is in the project ?

Everything is build around react and some mobx principles.

There are 4 components in the project : 
- A simple HelloComponent, with a button calling an action on HelloState <br />
- A ListComponent getting a list of words that you can interact with from his own state <br />
- A Generic FieldComponent working with the ListComponent, stateless working with props provided from List Component,
    The goal Was to be able to implement a special kind of fields and prepare for the fun stuff ( verifications etc ... ) <br />
- A StarWarsComponent : Getting information from his own state with actions calling the api and check errors etc ... 

Each states should not ( but can ) be modified with other means than the actions to keep the state safe in any cases
Each states can be tested ( cf : starwars.test.ts --> StarwarsState , list.state.test.ts --> ListState)
It is possible to have an app scale state even if it's heavy ( like redux xD )
 
starwars.service.ts --> service using the wrapping made in : rxjs.config.ts to call the api <br />
    Here, fetchCharacters$ --> is the observable to call to be able to get the list of characters from the api  <br />
    --> it's separated from fetchCharacters() to be able to test the service & the action separately ( cf : starwars.test.ts ) <br />``````


## Available Scripts

In the project directory, you can run:

### `npm start`
### `npm test`
### `npm run build`

## Learn More

To Check Create React AppComponent, [Documentation](https://facebook.github.io/create-react-app/docs/getting-started). <br />
To learn React, [Documentation](https://reactjs.org/). <br />
To learn Jestjs, [Documentation](https://jestjs.io/docs/en/getting-started). <br />
To learn Axios, [Github](https://github.com/axios/axios). <br />
To learn rxjs, [Website](https://rxjs-dev.firebaseapp.com/guide/overview). <br />
To learn mobx, [Website](https://mobx.js.org/README.html). <br />
Good Tutorial to get started with mobx [React Mobx Typescript Tuto](https://egghead.io/lessons/react-react-with-mobx-and-typescript-course-introduction). <br />

MobX devtools : [Github](https://github.com/mobxjs/mobx-devtools). <br />

Nice to have , FormState: [Github](https://formstate.github.io/#/). <br />

##Todo 
- Check Mobx State Tree 
- Check reactions of mobx

##Update 1 
- Add react router binding to a store : cf --> router.component.tsx + router.state.ts
- add example of functional component : cf --> starwars.component.tsx
- add example of global store : cf --> index.tsx + app.component.tsx
